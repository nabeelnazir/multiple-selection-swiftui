//
//  Name.swift
//  MultipleSelection
//
//  Created by Nabeel Nazir on 06/09/2022.
//

import Foundation

class NameItem: ObservableObject {
    var id: Int
    var name: String
    var isChecked: Bool
    
    init(id: Int, name: String, isChecked: Bool) {
        self.id = id
        self.name = name
        self.isChecked = isChecked
    }
}


class Names: ObservableObject {
    @Published var items = [
        NameItem(id: 1, name: "Nabeel", isChecked: false),
        NameItem(id: 2, name: "Adeel", isChecked: false),
        NameItem(id: 3, name: "Junaid", isChecked: false),
        NameItem(id: 4, name: "Uzair", isChecked: false),
        NameItem(id: 5, name: "Ali", isChecked: false)
    ]
}
