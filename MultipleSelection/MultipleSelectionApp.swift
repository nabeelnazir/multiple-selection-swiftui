//
//  MultipleSelectionApp.swift
//  MultipleSelection
//
//  Created by Nabeel Nazir on 06/09/2022.
//

import SwiftUI

@main
struct MultipleSelectionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
