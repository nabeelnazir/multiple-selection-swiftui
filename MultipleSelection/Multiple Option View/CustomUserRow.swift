//
//  CustomUserRow.swift
//  MultipleSelection
//
//  Created by Nabeel Nazir on 06/09/2022.
//

import SwiftUI

struct CustomUserRow: View {
    @Binding var isChecked: Bool
    var name: String!
    
    var body: some View {
        
        HStack(alignment: .center, spacing: 10) {
            CustomCheckBox(checked: $isChecked)
            
            Text(name)
                .foregroundColor(Color.black)
                .font(Font.system(size: 18))
        }.padding(10)
            .foregroundColor(.white)
    }
}

struct CustomUserRow_Previews: PreviewProvider {
    @State static var checked = false

    static var previews: some View {
        CustomUserRow(isChecked: $checked)
    }
}
