//
//  CustomCheckBox.swift
//  MultipleSelection
//
//  Created by Nabeel Nazir on 06/09/2022.
//

import SwiftUI

struct CustomCheckBox: View {
    @Binding var checked: Bool
    
    var body: some View {
        Image(systemName: checked ? "checkmark.square.fill" : "square")
            .foregroundColor(Color.blue)
            .onTapGesture {
                self.checked.toggle()
            }
    }
}

struct CustomCheckBoc_Previews: PreviewProvider {
    @State static var checked = false
            
    static var previews: some View {
        CustomCheckBox(checked: $checked)
    }
}
