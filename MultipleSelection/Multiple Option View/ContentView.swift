//
//  ContentView.swift
//  MultipleSelection
//
//  Created by Nabeel Nazir on 06/09/2022.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var names = Names()
    
    var body: some View {
        VStack(alignment: .leading, spacing: 30) {
            let selectedCount = names.items.filter({ $0.isChecked }).count
            var isAllSelected = selectedCount == names.items.count
            
            let myTitle = isAllSelected ? "All Selected" : "\(selectedCount) Selected"
            
            let bindingAllSelected = Binding<Bool>(get: {return isAllSelected},
                                  set: { p in isAllSelected = p})
            
            CustomUserRow(isChecked: bindingAllSelected, name: myTitle)
            .padding(.leading, 20)
                        
            List() {
                ForEach($names.items, id: \.id) { name in
                    CustomUserRow(isChecked: name.isChecked, name: name.name.wrappedValue )
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    @State static var ids = [Int]()

    static var previews: some View {
        ContentView()
    }
}

extension Binding where Value == Bool {
    var not: Binding<Value> {
        Binding<Value>(
            get: { !self.wrappedValue },
            set: { self.wrappedValue = !$0 }
        )
    }
}
